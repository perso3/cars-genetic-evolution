import pygame
from BasicVector import Vec2
from pygame.locals import *
import time

from car import Car
from renderer import render

from functions import *

def simulate():
    WINDOW_SIZE = (600, 600)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    goal = Vec2(100, 550)

    generation = 0

    step = 0

    cars = []

    for i in range(100):
        cars.append(Car(Vec2(300, 0), [], [], 0.01, False))

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("")
    mainClock = pygame.time.Clock()

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    pass

        if not pause:
            pass

        if frameCount % 1 == 0:
            if step == 300:
                cars = new_generation(cars, goal, 0.1)
                step = 0

            for car in cars:
                car.update()

            step += 1

            render(window, cars)
        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()