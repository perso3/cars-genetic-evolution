from BasicVector import Vec2
import random


class Car:

    def __init__(self, start_pos, speeds, directions, mutation_rate, parent):
        self.parent = parent
        self.pos = start_pos
        self.speeds = speeds
        self.directions = directions
        self.mutation_rate = mutation_rate
        self.index = 0

        self.last_direction = 90

    def update(self):
        if not self.parent:
            direction = Vec2.degreesToVec2(self.last_direction + random.randrange(-30, 30))
            speed = random.randrange(0, 40) / 10

            step = direction * speed

            self.pos += step

            self.directions.append(direction)
            self.speeds.append(speed)

            self.last_direction = Vec2.vec2ToDegrees(direction)

        else:

            speed = self.speeds[self.index]
            direction = self.directions[self.index]

            if random.randrange(0, 100) / 100 == self.mutation_rate:
                direction = Vec2.degreesToVec2(self.last_direction + random.randrange(-30, 30))
                speed = random.randrange(0, 40) / 10

            step = direction * speed

            self.pos += step

            self.directions.append(direction)
            self.speeds.append(speed)

            self.index += 1

            self.last_direction = Vec2.vec2ToDegrees(direction)

    def score(self, goal_pos):
        return Vec2.dist(self.pos, goal_pos)
