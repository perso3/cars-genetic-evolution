from car import Car
from BasicVector import Vec2

def scores(cars, goal):
    res = {}

    for car in cars:
        res[car] = car.score(goal)

    res = sorted(res, key=res.get, reverse=False)

    return res


def new_generation(cars, goal, best_percentage):
    car_count = len(cars)
    best_count = int(car_count * best_percentage)
    res = []
    scoreboard_cars = scores(cars, goal)
    best = scoreboard_cars[0:best_count]

    print(scoreboard_cars[0].score(goal))

    for first_parent in best:
        for second_parent in best:
            lists_len = len(first_parent.directions)
            directions = first_parent.directions[0:lists_len] + second_parent.directions[lists_len:]
            speeds = first_parent.speeds[0:lists_len] + second_parent.speeds[lists_len:]

            res.append(Car(Vec2(300, 0), speeds, directions, 0.5, True))

    # for _ in range(car_count):
    #     pass

    return res
