import pygame


def clear(window):
    window.fill((0, 0, 0))


def render(window, cars):
    clear(window)

    for car in cars:
        color = (255, 0, 0)

        pygame.draw.circle(window, color, car.pos.getPos(), 5)
